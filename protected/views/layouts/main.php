<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<meta name="keywords" content="free design template, download website templates, Floral Service Website, XHTML, CSS" />
<meta name="description" content="Floral Service - Free Website Template, Free XHTML CSS Design Layout" />
<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/templatemo_style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/js/slimbox2.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/slimbox2.js"></script>
</head>
<body>
	<div id="templatemo_top_bg">
    <!-- Free Website Template from www.TemplateMo.com -->
    	<div class="templatemo_container">
        	<div id="templatemo_logo">
            	Your Slogan Text Goes Here
            </div>
            <div id="templatemo_menu_section">
				<ul>
		            <li><a href="<?php echo YiiBase::app()->homeUrl;?>" class="current">Home</a></li>
        		    <li><a href="<?php echo YiiBase::app()->homeUrl;?>/site/newslist/">公告</a> </li>
		            <li><a href="#">产品</a></li>
		            <li><a href="#">服务</a></li>
                	<li><a href="#">留言</a></li>  
	        	</ul>
			</div><!-- End Of Menu Section -->
        </div><!-- End Of Container -->
    </div><!-- End Of Top Background -->
    
    <?php echo $content;?>
<!-- End Of Middle Bg -->
    <div id="templatemo_footer_center">
    	<div id="templatemo_footer">Copyright © <?php echo date('Y'); ?> | Power by <a href="<?php echo YiiBase::app()->homeUrl;?>/Admin/">Young</a></div>
    </div>
</body>
</html>