    <div id="templatemo_middle_bg">
    	<div class="templatemo_container">
        	<div id="templatemo_welcome_sec">
            	<div id="templatemo_welcome_text">
                	<span class="header_green">
                    	Welcome to 
                    </span>
                    <span class="header_orange">
                    	our Floral Site
                    </span>
                    <?php 
						foreach($dataAbout as $about){
							$beseUrl = Yii::app()->request->baseUrl;
							echo "<h1>$about->name</h1>";
							echo "<img alt='Flower' src='" . $beseUrl . "/images/templatemo_thumb.jpg' />";
							echo $about->content;
						}
					?>
                    
                </div>
            </div><!-- End Of Welcome section -->
        </div><!-- End Of Container -->
    </div><!-- End Of Middle Bg -->
    
    <div id="templatemo_bottom_bg">
    	<div class="templatemo_container">
	    	<div id="templatemo_content_area">
				<div id="templatemo_left_section">
                	<div class="templatemo_section_box">
						<h1>公告</h1>
						<?php
							foreach($dataNewsList as $newslist){
								echo "<h2>".CHtml::link($newslist['news_title'], array('tpage', 'id'=>$newslist['id']), array('target'=>'_blank'))."</h2>".$newslist->news_content;
							}			
						?>
					</div>
                </div><!-- end of left section -->
                <div id="templatemo_right_section">
                	<div class="templatemo_textarea">
	                    <?php 
							foreach($dataServices as $services){
								echo "<h1>$services->name</h1>";
								echo $services->content;
								}
						?>
					</div><!-- End of text area -->
					<div class="templatemo_gallery">
                		<div class="templatemo_gallery_header">
                        	Our Photo Gallery</div>
                        <div class="templatemo_gallery_body">
                        	<a href="<?php echo Yii::app()->request->baseUrl; ?>/images/templatemo_thumb_01_big.jpg" rel="lightbox-gallery" title="Lindl">
	                        	<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/templatemo_thumb_01.jpg" alt="Lindl" border="0" />
							</a>
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/images/templatemo_thumb_02_big.jpg" rel="lightbox-gallery" title="violet color Lindl">
	                        	<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/templatemo_thumb_02.jpg" alt="violet color Lindl"/>
							</a>
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/images/templatemo_thumb_03_big.jpg" rel="lightbox-gallery" title="Flower">
	                        	<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/templatemo_thumb_03.jpg" alt="Flower"/>
							</a>
                            <a href="<?php echo Yii::app()->request->baseUrl; ?>/images/templatemo_thumb_04_big.jpg" rel="lightbox-gallery" title="Orchid">
	                        	<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/templatemo_thumb_04.jpg" alt="Orchid"/>
							</a>
                        </div>
                    </div>
                    <div class="templatemo_textarea">
                    	<h1>W3C Validated</h1>
    	                <p>
	                    	<a href="http://validator.w3.org/check?uri=referer"><img src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0 Transitional" width="88" height="31" border="0" /></a>
							<a href="http://jigsaw.w3.org/css-validator/check/referer"><img style="border:0;width:88px;height:31px" alt="Valid CSS" src="http://jigsaw.w3.org/css-validator/images/vcss-blue" /></a>
						</p>
      					<div class="cleaner"></div>
                    </div>
                </div><!-- End of Right section -->  
                <div class="cleaner"></div>   
                
    		</div><!-- End Of Content area -->
		</div><!-- End Of Container -->
    </div>