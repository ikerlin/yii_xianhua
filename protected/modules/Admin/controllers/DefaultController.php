<?php

class DefaultController extends Controller
{
	public $layout='//layouts/column2';
	
	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
				'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	public function accessRules()
	{
		return array(
				array('allow',
						'actions'=>array('login','logout','captcha'),
						'users'=>array('*'),
				),
				array('allow', 
						'actions'=>array('index'),
						'users'=>array('@'),
				),
				array('deny',  // deny all users
						'users'=>array('*'),
				),
		);
	}
	
	public function actionIndex()
	{
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	public function actions() {  
	
		return array(  
	
			'captcha'=> array(  
	
				'class'=>'CCaptchaAction',
	
				'width'=>140,    //默认120
	
				'height'=>50,    //默认50
	
				'padding'=>2,    //文字周边填充大小
	
				'backColor'=>0xFFFFFF,      //背景颜色
	
				'foreColor'=>0x2040A0,     //字体颜色
	
				'minLength'=>6,      //设置最短为6位
	
				'maxLength'=>7,       //设置最长为7位,生成的code在6-7直接rand了
	
				'transparent'=>false,      //显示为透明,默认中可以看到为false
	
				'offset'=>-2,        //设置字符偏移量
	
// 				'controller'=>'admin',        //拥有这个动作的controller
	
	   ));  
	
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;
	
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	
		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
		
	}
	
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}