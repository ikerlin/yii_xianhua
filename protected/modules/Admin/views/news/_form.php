<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'news-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'news_title'); ?>
		<?php echo $form->textField($model,'news_title',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'news_title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'news_content'); ?>
		<?php $this->widget('application.extensions.editor.CKkceditor',array(
		    "model"=>$model,                # Data-Model
		    "attribute"=>'news_content',         # Attribute in the Data-Model
		    "height"=>'400px',
		    "width"=>'100%',
			"filespath"=>Yii::app()->basePath."/../up/",
			"filesurl"=>Yii::app()->baseUrl."/up/",
		) ); ?>
		<?php echo $form->error($model,'news_content'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? '创建' : '保存'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->