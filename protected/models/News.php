<?php

/**
 * This is the model class for table "{{_news}}".
 *
 * The followings are the available columns in table '{{_news}}':
 * @property string $id
 * @property string $news_title
 * @property string $news_content
 * @property string $create_time
 * @property string $update_time
 */
class News extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{news}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('news_title, news_content', 'required'),
			array('news_title', 'length', 'max'=>128),
			array('news_content', 'length', 'max'=>8000),
			array('create_time,update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, news_title, news_content, create_time, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'news_title' => '标题',
			'news_content' => '内容',
			'create_time' => '创建时间',
			'update_time' => '更新时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('news_title',$this->news_title,true);
		$criteria->compare('news_content',$this->news_content,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function beforeSave()
	{
		if (parent::beforeSave()) {
			if ($this->isNewRecord) {
				// add a new news
				$this->create_time = date('Y-m-d H:i:s');
			}else {
				// update a news
				$this->update_time = date('Y-m-d H:i:s');
			}
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * 倒序返回4条公告
	 */
	public function getNewsList()
	{
		$results = News::model()->findAll(array('order' => 'create_time desc','limit' => 4,'offset' => 0));
		return $results;
	}
	
	/**
	 * 倒序返回所有公告列表
	 */
	public function getNewsListAll()
	{
		$results = News::model()->findAll(array('order' => 'create_time desc'));
		return $results;
	}
	
	/**
	 * 根据ID返回公告具体内容
	 */
	public function getNewsById ($id = 1)
	{
		$id = (int)$id;
		$results = News::model()->findAll('id = :id order by create_time DESC limit 0, 1',array(':id'=>$id));
// 		$results = News::model()->findAll('type_id = :type_id and status_id=1 order by create_time DESC limit 0, 7',array(':type_id'=>$type_id));
		return $results;
	}
}