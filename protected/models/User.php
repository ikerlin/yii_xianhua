<?php

/**
 * This is the model class for table "{{_user}}".
 *
 * The followings are the available columns in table '{{_user}}':
 * @property string $id
 * @property string $username
 * @property string $password
 * @property string $tel
 * @property string $sex
 * @property string $class_id
 * @property integer $status_id
 * @property string $create_time
 * @property string $update_time
 * @property string $create_user_id
 * @property string $update_user_id
 */
class User extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, sex, class_id', 'required'),
			array('status_id', 'numerical', 'integerOnly'=>true),
			array('username', 'length', 'max'=>64),
			array('password', 'length', 'max'=>32),
			array('tel', 'length', 'max'=>11),
			array('sex', 'length', 'max'=>1),
			array('class_id, create_user_id, update_user_id', 'length', 'max'=>10),
			array('update_time, status_id, create_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, password, tel, sex, class_id, status_id, create_time, update_time, create_user_id, update_user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => '用户名',
			'password' => '密码',
			'tel' => '电话',
			'sex' => '性别',
			'class_id' => '类别',
			'status_id' => '状态',
			'create_time' => '创建时间',
			'update_time' => '更新时间',
			'create_user_id' => '创建人',
			'update_user_id' => '更新人',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('tel',$this->tel,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('class_id',$this->class_id,true);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('create_user_id',$this->create_user_id,true);
		$criteria->compare('update_user_id',$this->update_user_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * Validate Password
	 * @param string $password
	 * @return boolean true/false
	 */
	public function validatePassword($password)
	{
		return $this->encrypt($password)===$this->password;
	}
	
	/**
	 * return md5 string of the input one.
	 * @param string $pass
	 */
	public function encrypt($pass)
	{
		return md5($pass);
	}
	
	protected function beforeSave()
	{
		if (parent::beforeSave()) {
				
			// update password use encrypt method
			// 如果是新纪录就内容加密
			/* if($this->isNewRecord){
				$this->password = $this->encrypt($this->password);
			} */
			$this->password = $this->encrypt($this->password);
			return true;
		}else {
			return false;
		}
	}
}