<?php

/**
 * This is the model class for table "{{_product}}".
 *
 * The followings are the available columns in table '{{_product}}':
 * @property string $id
 * @property string $product_title
 * @property string $product_content
 * @property string $product_pic
 * @property string $author_name
 * @property string $create_time
 * @property string $update_time
 * @property string $create_user_id
 * @property string $update_user_id
 */
class Product extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	
	public $image;
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{product}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//文件上传类型			
			array('image', 'file', 'types'=>'jpg, gif, png'),
			array('product_title, product_content', 'required'),
			array('product_title', 'length', 'max'=>128),
			array('product_content', 'length', 'max'=>8000),
			array('product_pic', 'length', 'max'=>200),
			array('author_name, create_user_id, update_user_id', 'length', 'max'=>10),
			array('update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, product_title, product_content, product_pic, author_name, create_time, update_time, create_user_id, update_user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'product_title' => 'Product Title',
			'product_content' => 'Product Content',
			'product_pic' => 'Product Pic',
			'author_name' => 'Author Name',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_user_id' => 'Create User',
			'update_user_id' => 'Update User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('product_title',$this->product_title,true);
		$criteria->compare('product_content',$this->product_content,true);
		$criteria->compare('product_pic',$this->product_pic,true);
		$criteria->compare('author_name',$this->author_name,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('create_user_id',$this->create_user_id,true);
		$criteria->compare('update_user_id',$this->update_user_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * 生成创建时间和更新时间
	 * 
	 */
	protected function beforeSave()
	{
		if (parent::beforeSave()) {
			if ($this->isNewRecord) {
				// add a new news
				$this->create_time = date('Y-m-d H:i:s');
			}else {
				// update a news
				$this->update_time = date('Y-m-d H:i:s');
			}
			return true;
		}else {
			return false;
		}
	}
}