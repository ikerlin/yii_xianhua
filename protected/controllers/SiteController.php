<?php

class SiteController extends Controller
{
	public $layout='//layouts/column1';
	
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// 获取关于我们
		$dataAbout = About::model()->getAbout();
		
		// 获取首页公告
		$dataNewsList = News::model()->getNewsList();
		
		// 获取服务介绍
		$dataServices = Services::model()->getServices();
		
		// 渲染默认视图
		$this->render('index', array('dataNewsList'=>$dataNewsList, 'dataServices'=>$dataServices, 'dataAbout'=>$dataAbout));
	}
	
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	/**
	 *  公告详细内容页数据获取、渲染
	 */
	public function actionTpage($id)
	{
		$dataNews = News::model()->getNewsById($id);
		$this->render('tpage', array('dataNews'=>$dataNews));
	}
	
	/**
	 * 公告列表页
	 */
	public function actionNewsList()
	{
		$newsListAll = News::model()->getNewsListAll();
// 		print_r($newsListAll);
// 		exit();
		$this->render('newslist', array('dataNewsListAll'=>$newsListAll));
	}
}