/*
Navicat MySQL Data Transfer

Source Server         : phpdev_mysql5.1.57
Source Server Version : 50157
Source Host           : localhost:3306
Source Database       : yii_xianhua

Target Server Type    : MYSQL
Target Server Version : 50157
File Encoding         : 65001

Date: 2014-06-21 11:01:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_about`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_about`;
CREATE TABLE `tbl_about` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `content` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_about
-- ----------------------------
INSERT INTO `tbl_about` VALUES ('1', '关于我们', '<p>\r\n	This is a free website layout provided by Sean. You may download, edit and apply this layout for your websites. Credit goes to <a href=\"http://www.baidu.com\" target=\"_blank\">Free Photos</a> for photos used in this template.</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consectetur adip iscing elit. Vesti bulum in mauris ultrices risus vive rra plac erat a vitae nulla. Proin convallis, magna at molestie mole stie.</p>\r\n');

-- ----------------------------
-- Table structure for `tbl_news`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_news`;
CREATE TABLE `tbl_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `news_title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `news_content` varchar(8000) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_news
-- ----------------------------
INSERT INTO `tbl_news` VALUES ('1', '公告1', '<p>\r\n	11111111111111111111111111</p>\r\n', '2013-08-29 11:31:22', null);
INSERT INTO `tbl_news` VALUES ('2', '各种放假', '<p>\r\n	222222222222222222</p>\r\n', '2013-08-29 11:31:35', '2013-08-30 10:55:03');
INSERT INTO `tbl_news` VALUES ('3', '放假', '<p>\r\n	33333333333333333</p>\r\n', '2013-08-29 11:31:47', '2013-08-30 10:54:41');
INSERT INTO `tbl_news` VALUES ('4', '中秋节放假', '<p>\r\n	444444444444444444444</p>\r\n', '2013-08-29 11:31:58', '2013-08-30 10:54:27');
INSERT INTO `tbl_news` VALUES ('5', '国庆节放假', '<p>\r\n	555公告内容555公告内容555公告内容555公告内容555公告内容555公告内容555公告内容555公告内容555公告内容555公告内容</p>\r\n', '2013-08-29 11:32:09', '2013-08-30 10:54:10');

-- ----------------------------
-- Table structure for `tbl_product`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_product`;
CREATE TABLE `tbl_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `product_content` varchar(8000) COLLATE utf8_unicode_ci NOT NULL,
  `product_pic` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_name` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` int(10) unsigned DEFAULT NULL,
  `update_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_product
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_services`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_services`;
CREATE TABLE `tbl_services` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_services
-- ----------------------------
INSERT INTO `tbl_services` VALUES ('1', '我们的服务', '<p>\r\n	<img alt=\"Flower\" src=\"/yii_xianhua/up/images/thumb_05.jpg\" />服务1介绍。服务1介绍。服务1介绍。服务1介绍。服务1介绍。服务1介绍。服务1介绍。服务1介绍。</p>\r\n<p>\r\n	服务2介绍。服务2介绍。服务2介绍。服务2介绍。服务2介绍。服务2介绍。服务2介绍。服务2介绍。服务2介绍。服务2介绍。</p>\r\n<p>\r\n	服务3介绍。服务3介绍。服务3介绍。服务3介绍。服务3介绍。服务3介绍。服务3介绍。服务3介绍。服务3介绍。服务3介绍。服务3介绍。服务3介绍。服务3介绍。服务3介绍。服务3介绍。服务3介绍。</p>\r\n');

-- ----------------------------
-- Table structure for `tbl_user`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `tel` char(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sex` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `status_id` tinyint(3) unsigned DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user_id` int(10) unsigned DEFAULT NULL,
  `update_user_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
INSERT INTO `tbl_user` VALUES ('6', 'admin', '21232f297a57a5a743894a0e4a801fc3', '', '1', '1', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '0');
